#!/usr/bin/env python3
## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

"""
  This should emit all polyhedra that folding can fold in one piece.
  If you find another one, call Manu.

  Just FYI: This currently does not work.
            I (Fabian), think we need to add constraints on the ratio of imin/jmin/kmin and the maxs respectively.
"""

import argparse
import sys

i_min: int
j_min: int
k_min: int
i_cnt: int
j_cnt: int
k_cnt: int
i_inc: int
j_inc: int
k_inc: int


def main(argv):
  parser = argparse.ArgumentParser()

  parser.add_argument(
    '-c', '--context', '--ctx',
    default='',
  )

  for n in 'ijk':
    parser.add_argument(f'-{n}-min', type=int, default=0)
    parser.add_argument(f'-{n}-cnt', type=int, default=1)
    parser.add_argument(f'-{n}-inc', type=int, default=1)

  # parser.add_argument('--emacs', type=int, dest='-i-cnt')

  for n in range(1, 7):
    parser.add_argument(f'-d{n}', type=int, default=0)

  args = parser.parse_args(argv)

  for n in 'ijk':
    globals()[f'{n}_min'] = getattr(args, f'{n}_min')
    globals()[f'{n}_cnt'] = getattr(args, f'{n}_cnt')
    globals()[f'{n}_inc'] = getattr(args, f'{n}_inc')

  for n in range(1, 7):
    globals()[f'd{n}'] = getattr(args, f'd{n}')


  for ci in range(i_cnt):
    i = i_min + i_inc * ci
    print('# BGN i', i)

    for cj in range(j_cnt + ci * (-d1 + d2)):
      j = j_min + j_inc * (cj + ci * d1)
      print('#   BGN j', j)

      for ck in range(k_cnt + ci * (-d3 + d4) + cj * (-d5 + d6)):
        k = k_min + k_inc * (ck + ci * d3 + cj * d5)
        print('#     BGN k', k)
        print(args.context + ':', ','.join(str(c) for c in [ci, cj, ck]), '--->', 0)

      print('#   END j', j)

    print('# END i', i)


if __name__ == '__main__':
  argv = list(sys.argv[1:])

  while argv:
    try:
      sep_idx = argv.index('--')
    except ValueError:
      sep_idx = len(argv)

    arg_group = argv[:sep_idx]
    argv      = argv[sep_idx + 1:]

    main(arg_group)
