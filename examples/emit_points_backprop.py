#!/usr/bin/env python3
## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

import argparse
import random
import sys


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c', '--context', '--ctx',
        default='',
    )
    parser.add_argument(
        '-n2',
        help='size of outer loop)',
        type=int,
        required=True
    )
    parser.add_argument(
        '-n1',
        help='size of inner loop',
        type=int,
        required=True
    )
    parser.add_argument('--contiguous_memory', '-cm',
        help = 'is memory contiguous',
        action = 'store_true',
        default = False
    )
    parser.add_argument('--by_row', '-br',
        help = 'is matrix stored by row',
        action = 'store_true',
        default = False
    )

    args = parser.parse_args()

    n2 = args.n2 or 0
    n1 = args.n1 or 0

    if args.by_row:
        label = 67
        offsets = [random.randrange(50, 100) for _ in range(n2)]
        for cj in range(n2):
            for ck in range(n1):
                print(args.context + ':' + str(cj) + ',' + str(ck) + ' --->', label)
                label += 4
            if not args.contiguous_memory:
                label += offsets[cj]
    else:
        for j in range(1, n2 + 1):
            cj = j-1
            for k in range(0, n1 + 1):
                ck = k
                if args.contiguous_memory:
                    kstep = 4
                else:
                    kstep = 4 + k % 4
                label = cj + kstep * ck + 67
                print(args.context + ':' + str(cj) + ',' + str(ck) + ' --->', label)


if __name__ == '__main__':
    main()
