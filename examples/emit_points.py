#!/usr/bin/env python3
## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

import argparse
import random
import sys


def main(argv):
  parser = argparse.ArgumentParser()

  parser.add_argument(
    '-c', '--context', '--ctx',
    default='',
  )

  parser.add_argument(
    '-g', '--geometry',
    choices=['cube', 'triangle', 'inverse-triangle'],
    default='cube',
  )

  parser.add_argument(
    '-x',
    help='size of shape in first dimension (x/k)',
    type=int,
  )
  parser.add_argument(
    '-y',
    help='size of shape in second dimension (y/j)',
    type=int,
  )
  parser.add_argument(
    '-z',
    help='size of shape in third dimension (z/i)',
    type=int,
  )

  parser.add_argument(
    '-l', '--label',
    choices=['trivial', 'affine', 'random'],
    help='trivial ... all labels are 0, affine ... labels are affine, random ... labels are random',
    default='trivial',
  )

  parser.add_argument(
    '-p', '--pieces',
    type=int, default=1,
  )

  args = parser.parse_args(argv)

  X = args.x or 0
  Y = args.y or 0
  Z = args.z or 0

  dimensions = 0
  for d in [X, Y, Z]:
    if d:
      dimensions += 1
    if d < 0:
      parser.error(f'dimension is negative ({d})')

  if dimensions == 0:
    parser.error('must specify at least one dimension (-x, -y, -z)')

  def gen_coords_and_label():
    for z in range(Z + 1):
      for y in range(Y + 1):
        for x in range(X + 1):
          if args.geometry == 'triangle' and ((Z and y < z) or (Y and x < y)):
            continue

          if args.geometry == 'inverse-triangle' and ((Z and y >= z) or (Y and x >= y)):
            continue

          coords = []
          for dim, val in [(Z, z), (Y, y), (X, x)]:
            if dim:
              coords.append(val)

          if args.label == 'trivial':
            label = 0
          elif args.label == 'affine':
            label = z * Y + y * X + x
          elif args.label == 'random':
            label = random.randint(0, 66)
          else:
            parser.error(f'invalid geometry: {args.label!r}')

          yield coords, label

  for piece in range(args.pieces):
    print('# piece', piece)

    for coords, label in gen_coords_and_label():
      tmp = list(coords)
      tmp[-1] += piece * (max(Z, Y, X) + 2)
      coords = tuple(tmp)

      print(args.context + ':', ','.join(str(c) for c in coords), '--->', label)


if __name__ == '__main__':
  argv = list(sys.argv[1:])

  while argv:
    try:
      sep_idx = argv.index('--')
    except ValueError:
      sep_idx = len(argv)

    arg_group = argv[:sep_idx]
    argv      = argv[sep_idx + 1:]

    main(arg_group)
