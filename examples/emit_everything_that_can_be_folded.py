#!/usr/bin/env python3
## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

"""
  This should emit all polyhedra that folding can fold in one piece.
  If you find another one, call Manu.

  Just FYI: This currently does not work.
            I (Fabian), think we need to add constraints on the ratio of imin/jmin/kmin and the maxs respectively.
"""

import argparse
import sys

i_min: int
j_min: int
k_min: int
i_max: int
j_max: int
k_max: int
i_inc: int
j_inc: int
k_inc: int


def main(argv):
  parser = argparse.ArgumentParser()

  parser.add_argument(
    '-c', '--context', '--ctx',
    default='',
  )

  for n in 'ijk':
    parser.add_argument(f'-{n}-min', type=int, default=0)
    parser.add_argument(f'-{n}-max', type=int, default=0)
    parser.add_argument(f'-{n}-inc', type=int, default=1)

  # parser.add_argument('--emacs', type=int, dest='-i-max')

  for n in range(1, 7):
    parser.add_argument(f'-d{n}', type=int, default=0)

  args = parser.parse_args(argv)

  for n in 'ijk':
    globals()[f'{n}_min'] = getattr(args, f'{n}_min')
    globals()[f'{n}_max'] = getattr(args, f'{n}_max')
    globals()[f'{n}_inc'] = getattr(args, f'{n}_inc')

  for n in range(1, 7):
    globals()[f'd{n}'] = getattr(args, f'd{n}')

  k = 0

  ci = 0
  for i in range(i_min, i_max + 1, i_inc):
    print('# BGN i', i)

    cj = 0
    for j in range(j_min + d1 * i, j_max + d2 * i + 1, j_inc):
      print('#   BGN j', j)

      ck = 0
      for k in range(k_min + d3 * i + d4 * j, k_max + d5 * i + d6 * j + 1, k_inc):
        print('#     BGN k', k)
        print(args.context + ':', ','.join(str(c) for c in [ci, cj, ck]), '--->', 0)
        ck += 1

      print('#   END j', j)
      cj += 1

    print('# END i', i)

    ci += 1


if __name__ == '__main__':
  argv = list(sys.argv[1:])

  while argv:
    try:
      sep_idx = argv.index('--')
    except ValueError:
      sep_idx = len(argv)

    arg_group = argv[:sep_idx]
    argv      = argv[sep_idx + 1:]

    main(arg_group)
