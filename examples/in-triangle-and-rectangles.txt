## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

## format: z,y,x ---> label

# two points that merge to line A.
0,0,0 ---> 0
0,0,1 ---> 28

# two points that merge to line B.
0,0,3 ---> 1
0,0,4 ---> 29

# two points that merge to line C.
0,1,0 ---> 65
0,1,1 ---> 113

# two points that merge to line D.
0,1,3 ---> 2
0,1,4 ---> 30

# end second row
# line B and line D merge to rectangle B-D

# two points that merge to line E
0,2,0 ---> 66
0,2,1 ---> 114

# two points that merge to line F
0,2,3 ---> 3
0,2,4 ---> 31

# end third row
# line C and E merge to rectangle C-E
# rectangle B-D absorbs line F

# one point that becomes line G
1,0,0 ---> 67

# end fourth row
# line A merges with line G

#3,3 ---> 115
#
#4,0 ---> 4
#4,1 ---> 32
#4,2 ---> 68
#4,3 ---> 116
#
#5,0 ---> 5
#5,1 ---> 33
#5,2 ---> 69
#5,3 ---> 117
#
#6,0 ---> 6
#6,1 ---> 34
#6,2 ---> 70
#6,3 ---> 118

