## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

## format: y,x ---> label

0,0 ---> 4
0,1 ---> 7
0,2 ---> 10
0,3 ---> 13
1,0 ---> 6
1,1 ---> 9
1,2 ---> 12
1,3 ---> 15
2,0 ---> 8
2,1 ---> 11
2,2 ---> 14
2,3 ---> 17

