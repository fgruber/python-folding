## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

__all__ = [
  'Worklist',
]

from typing import *

T = TypeVar('T')


class Worklist(Generic[T]):
    """
        A FIFO queue
    """

    def __init__(self):
        self.list = []

    def push(self, node):
        """
            Append element (is the new youngest element)
        """
        assert node not in self.list
        self.list.append(node)

    def pop(self):
        """
            Remove the last (=youngest element) and return it.
        """
        assert self.list, 'pop on empty Worklist'
        return self.list.pop(-1)

    def remove(self, node):
        assert node in self.list, f'{node} not in {self.list}'
        self.list.remove(node)

    def splice(self, other: 'Worklist'):
        """
            Remove all elements from :other: and append them to :self:
        """
        assert self is not other
        self.list.extend(other.list)
        other.list.clear()

    def __bool__(self):
        return bool(self.list)

    def __iter__(self):
        """
            Iterate from front to back (oldest to youngest element)
        """
        return iter(list(self.list))

    def __repr__(self):
        return repr(self.list)
