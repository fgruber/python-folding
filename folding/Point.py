## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

__all__ = [
    'Point',
]

from folding.Vector      import Vector
from folding.NamedStruct import NamedStruct


class Point(NamedStruct, immutable = True):
    """
        Input for folding.
        A point knows its coordinate vector, its label vector.

        Treat these as immutable and don't assign any fields (makes other code simpler)
    """

    context: str
    IV: Vector
    labels: Vector

    @property
    def num_dimensions(self):
        return len(self.IV)

    @property
    def num_labels(self):
        return len(self.labels)

    def __repr__(self):
        return ','.join(map(str, self.IV)) + ' ---> ' + ','.join(map(str, self.labels))

    def __str__(self):
        return repr(self.IV)
        # return ','.join(map(str, self.IV)) + ' ---> ' + ','.join(map(str, self.labels))
