## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

__all__ = [
  'NamedStruct',
]

from typing import *


class NamedStruct:
    """
        Like typing.NamedTuple but with mutable fields
    """

    def __init_subclass__(clss, immutable: bool = False):
        def _instantiate(clss):
            try:
                return clss()
            except TypeError as e:
                if hasattr(clss, '_subs_tree'):
                    subs_tree = clss._subs_tree()
                    if len(subs_tree) == 3 and clss == Optional[clss._subs_tree()[1]]:
                        return None

                for builtin_type in [list, dict, set]:
                    if builtin_type in getattr(clss, 'mro', lambda: [])():
                        return builtin_type()

                raise e

        if immutable:
            def __setattr__(self, attr):
                raise AttributeError("can't set attribute")

            clss.__setattr__ = __setattr__

        def __init__(self, *args, **kwargs):
            types = get_type_hints(type(self))

            for val in args:
                var = next(iter(types))
                types.pop(var)
                object.__setattr__(self, var, val)

            for var, val in kwargs.items():
                types.pop(var)
                object.__setattr__(self, var, val)

            for var, var_type in types.items():
                if hasattr(clss, var):
                    object.__setattr__(self, var, getattr(clss, var))
                else:
                    object.__setattr__(self, var, _instantiate(var_type))

            __post_init__ = getattr(self, '__post_init__', None)
            if callable(__post_init__):
                __post_init__()

        clss.__init__ = __init__

        if clss.__repr__ == object.__repr__:
            def __repr__(self):
                txt = type(self).__name__
                txt += '('
                txt += ', '.join(f'{var}={getattr(self, var)}' for var in vars(self))
                txt += ')'
                return txt

            clss.__repr__ = __repr__
