## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

__all__ = [
  'Polyhedron',
]


from folding.Affine_Function import Affine_Function
from folding.Dimension       import Dimension
from folding.Point           import Point
from folding.NamedStruct     import NamedStruct
from folding.Vector          import Vector

import itertools
from typing import *

import numpy
import numpy.linalg


class Polyhedron(NamedStruct):
    """
        A polyhedron is represented by its convex hull.
        We only represent polyhedra in N dimensions that can be represented using 2^N extreme points.

        Each N dimensional polyhedron decomposes into two 'faces', i.e. two sub-polyhedra of dimension N-1.

        Layout of 'extreme_points':
          len(extreme_points) == 2 ** (dimension - 1)

          'extreme_points' holds a d-dimensional *elementary polyhedron*.
          This polyhedron is bounded by faces, which are themselves (d-1)-dimensional elementary polyhedra.
          (it is also implicitly bounded by 2 * (d-1) other *side faces* created by connecting the sides of the main two faces)
          The *lower face* is the face containing the lexicographically *lower* (i.e. older) points.
          The *upper face* is the face containing the lexicographically *upper* (i.e. younger) points.

          extreme_points is layed out as follows:
            [points of the upper face] + [points of the lower face]
    """

    extreme_points:     List[Vector]
    growing_directions: List[Vector]
    affine_functions:   List[Affine_Function]
    dimension:          Dimension

    @classmethod
    def from_point(clss, point: Point, widen_affine_functions: bool):
      IV     = point.IV
      labels = [Affine_Function(point, v, widen_affine_functions) for v in point.labels]
      dim    = 0

      return Polyhedron([IV], [], labels, dim)

    def __post_init__(self):
        assert self.extreme_points, 'empty poly'
        assert self.is_degenerate, 'polyhedron is not degenerate at time of creation'

        dim = self.dimension

        assert len(self.extreme_points) == 2 ** dim, f'number of extreme points is not a power of two ({self})'

    @property
    def is_degenerate(self):
        return not bool(self.growing_directions)

    def normalize(P: 'Polyhedron', d: Dimension):
        assert P.dimension == d - 1

        while P.dimension < d:
            q = P.dimension
            # NOTE: python slices don't include the last element
            P[2 ** q:2 ** (q + 1)] = P[0:2 ** q]
            P.dimension = q + 1

        P.growing_directions = None

    def canExtend(P1: 'Polyhedron', P2: 'Polyhedron') -> bool:
        """
            Check if P1 can be extended to contain all points of P2.

            P1 has d dimensions and may be degenerate along d.
            P2 has d-1 dimensions.
        """

        assert P1.dimension == P2.dimension + 1

        d = P1.dimension

        if P1.is_degenerate:
            ## merge -- a degenerate d-polytope absorbs another degenerate d-polytope

            for k in range(0, 2 ** (d - 1)):
                # we can absorb if all extreme point are connected by a d-dimensional search vector.

                diff = P2[k] - P1[k]

                if not diff.is_a_search_vector(d):
                    return False

            ## Check if merging P2 into P1 (i.e. calling `P1.extend(P2)`) forms a valid polyhedron.

            if d <= 2:
              ## This always holds in 1D and 2D, i.e.:
              ##  1D: any point is a valid point
              ##  2D: any two points form a valid line
              return True

            for side_face in Polyhedron._side_faces_of_merged_polyhedron(P1, P2, d):
              assert len(side_face) == 2 ** (d - 1)

              if not Polyhedron._all_points_lie_on_same_hyperplane(side_face, d):
                return False

            return True
        else:
            ## extension -- a non-degenerate d-polytope absorbs a degenerate d-polytope

            for k in range(0, 2 ** (d - 1)):
                v = P1.growing_directions[k]

                if P1[k] + v != P2[k]:
                    return False

            return True

    def extend(P1: 'Polyhedron', P2: 'Polyhedron', d: Dimension):
        """
            Extend P1 to contain all points of P2, we can then throw away P2.
        """

        assert P1.dimension == d
        assert P2.dimension == d - 1

        assert len(P1.extreme_points) == 2 * len(P2.extreme_points)

        if P1.is_degenerate:
            ## merge -- a degenerate d-polytope absorbs another degenerate d-polytope

            growing_directions = [None] * (len(P1.extreme_points) // 2)

            for i in range(0, 2 ** (d - 1)):
                assert P1[i] == P1[i + 2 ** (d - 1)], f'P1 ({P1}) is not really degenerate | {i} {i + 2 ** (d - 1)} | {P1[i]} == {P1[i + 2 ** (d - 1)]}'

                growing_directions[i] = dir = P2[i] - P1[i]
                assert dir.is_a_search_vector(d), dir

                P1[i] = P2[i]

            P1.growing_directions = growing_directions
        else:
            ## extension -- a non-degenerate d-polytope absorbs a degenerate d-polytope

            assert len(P1.extreme_points) == 2 * len(P1.growing_directions)

            for i in range(0, 2 ** (d - 1)):
                assert P1[i] + P1.growing_directions[i] == P2[i], f'{P1[i]} + {P1.growing_directions[i]} != {P2[i]}'
                P1[i] = P1[i] + P1.growing_directions[i]

    def __len__(self) -> int:
        return len(self.extreme_points)

    def __getitem__(self, idx: int) -> Vector:
        return self.extreme_points[idx]

    def __setitem__(self, idx: int, item: Vector):
        self.extreme_points[idx] = item

    def __str__(self) -> str:
        return '[' + ', '.join(map(str, self.extreme_points)) + '] --> ' + repr(self.affine_functions) + ''

    def __repr__(self) -> str:
        return ''.join([
          type(self).__name__, '(',
          '[', ', '.join(map(str, self.extreme_points)), ']',
          ' --> ',
          repr(self.affine_functions),
          ')'
        ])

    @classmethod
    def _side_faces_of_merged_polyhedron(clss, P1: 'Polyhedron',
                                         P2: 'Polyhedron', d: Dimension) -> Iterable[Tuple[Vector, ...]]:
      """
        Enumerate points of *side faces* of merge of polyhedra P1 & P2.
        I.e. the merge of P1 & P2 where P2 replaces the upper face of P1.
      """

      for face in clss._enumerate_faces_of_polyhedron(d - 1):
        points = []

        for index in face:
          points.append(P1[index])
          points.append(P2[index])

        yield points

    @classmethod
    def _all_points_lie_on_same_hyperplane(clss, points: Sequence[Vector], d: Dimension) -> bool:
      """
        Check if all points in :points: lie on the same :num_dimensions:-dimensional hyperplane.
      """

      assert len(points) == 2 ** (d - 1)

      ## Discard duplicate points
      ## NOTE: This changes the order of points
      ##       this should not matter, I hope?
      points = list(set(points))

      ## NOTE: We could maybe optimize this process a bit.
      ##       I'm pretty convinced that when the same point appears multiple times in a face
      ##       (i.e. when the polyhedron is degenerate along some dimension)
      ##       there is always at least one hyperplane containing all the remaining points.
      ##       Checking this would be cheaper than computing the normal vectors.

      ## 1. Take d+1 points and caculate hyperplane equation for plane containing them.
      ## 1.1. A hyperplane is defined by a point `origin` and a normal vector 'normal' which is orthogonal
      ##      to all vectors lying on the plane.
      ##      The plain then consists of all points `P` such that `dot(P - origin, normal) == 0`.
      origin       = points[0]
      plane_points = points[1:d + 1]
      other_points = points[d + 2:]

      ## 1.2. Find plane normal by calculating the null space of the space spanned by the vectors that start at origin
      ##      and go to the other points we want to find a plane for.
      vectors = numpy.array([p - origin for p in plane_points])

      ## This costs O(d^3), so avoid it if possible.
      normals = null_space(vectors)

      if normals.size == 0:
        ## Found no normal. First N+1 points do not lie on a hyperplane.
        return False

      assert normals.shape[0] == vectors.shape[1], "normal vector does not have some number of dimensions " \
                                                   "as ambient space?" \
                                                   f" Want {vectors.shape[1]}, have {normals.shape[0]})"
      assert normals.shape[1] >= 1, "found no normal vectors"

      ## NOTE: This can return multiple normal vectors.
      ##       We arbitrarily use the first one.
      ## FIXME: Is this OK??
      normal = normals.T[0]

      ## 2. Check if all the remaining points lie on the same hyperplane.
      for point in other_points:
        dot = numpy.dot(point - origin, normal)
        # NOTE: math.isclose(N, 0) == False for any N
        #       https://stackoverflow.com/a/35325039
        if abs(dot) > 1e-10:
          assert abs(dot) > 0.1, '%.20f (%g) != 0' % (dot, dot)
          return False

      return True

    @classmethod
    def _enumerate_faces_of_polyhedron(clss, d: Dimension) -> Tuple[Tuple[int, ...], ...]:
      """
        Enumerate all faces of a :d: dimensional elementary polyhedron.
        This actually returns the integer indices of all the points,
        to get the actual points you have to index into the extreme_points array.

        This method exploits the fact that folding always puts the extreme points in to the array in
        a predictable order.

        TODO: explain how this works, it is pretty funky.
      """

      oldest_point = 0
      newest_point = (2 ** d) - 1

      return tuple(
        clss._walk_face_in_dimension(start, d, dim) for dim in range(d) for start in (oldest_point, newest_point)
      )

    @classmethod
    def _walk_face_in_dimension(clss, node: int, num_dimensions: int, fixed_dim: int) -> Tuple[int, ...]:
      """
        Find indices of extreme points in a face of a n-dimensional elementary polyhedron starting at node :node:.
        If you reduce a d-dimensional elementary polyhedron to a d-dimensional hypercube (TODO: explain how that works)
        The returned face will be parallel to the hyperplane spanned by all axes except :fixed_dim:.
        I.e. in 3D, if :fixed_dim: == k, then we will enumerate a face parallel to the ij plane.
        If :fixed_dim: == j, then we will enumerate a face parallel to the ik plane.

              * i
              |
              |
              |
              *---------* k
             /
            /
           /
        j *

        In other words all points of the returned face will have the same coordinate along :fixed_dim: in the
        hypercube reduction of the elementary polyhedron.
      """

      worklist = []
      path     = [node]
      root     = node

      want_len = 2 ** (num_dimensions - 1)

      for i in range(want_len):
        for neighbour in clss._neighbours(path[-1], num_dimensions, fixed_dim):
          if neighbour in path:
            continue

          path.append(neighbour)
          break

      assert len(path) == want_len

      return tuple(path)

    @staticmethod
    def _neighbours(node: int, num_dimensions: int, fixed_dim: int) -> Iterable[int]:
      assert node.bit_length() <= num_dimensions

      for dim in range(num_dimensions):
        if dim == fixed_dim:
          continue

        yield node ^ (1 << dim)


def null_space(A):
  """
    Construct an orthonormal basis for the null space of A using SVD

    Parameters
    ----------
    A : (M, N) array_like
        Input array

    Returns
    -------
    Z : (N, K) ndarray
        Orthonormal basis for the null space of A.
        K = dimension of effective null space, as determined by rcond

    See also
    --------
    svd : Singular value decomposition of a matrix
    orth : Matrix range

    Examples
    --------
    One-dimensional null space:

    >>> from scipy.linalg import null_space
    >>> A = np.array([[1, 1], [1, 1]])
    >>> ns = null_space(A)
    >>> ns * np.sign(ns[0,0])  # Remove the sign ambiguity of the vector
    array([[ 0.70710678],
           [-0.70710678]])

    Two-dimensional null space:

    >>> B = np.random.rand(3, 5)
    >>> Z = null_space(B)
    >>> Z.shape
    (5, 2)
    >>> np.allclose(B.dot(Z), 0)
    True

    The basis vectors are orthonormal (up to rounding error):

    >>> Z.T.dot(Z)
    array([[  1.00000000e+00,   6.92087741e-17],
           [  6.92087741e-17,   1.00000000e+00]])

  """
  u, s, vh = numpy.linalg.svd(A, full_matrices=True)
  M, N = u.shape[0], vh.shape[1]

  rcond = numpy.finfo(s.dtype).eps * max(M, N)

  tol = numpy.amax(s) * rcond
  num = numpy.sum(s > tol, dtype=int)
  Q = vh[num:, :].T
  return Q
