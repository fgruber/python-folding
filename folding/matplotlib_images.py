## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

from folding.images import *
from folding.Vector import Vector
from typing import *
import matplotlib.pyplot as plt
import matplotlib.collections
import mpl_toolkits.mplot3d
import mpl_toolkits.mplot3d.art3d
import numpy as np
import enum


################################################################################
### LOW LEVEL PRINTING API


class Key_Input_Mode(enum.Enum):
  # change camera position or projection
  CHANGE_VIEW  = 'change view'
  # change number of slide currently being shown
  CHANGE_SLIDE = 'change slide'

  def next(self):
    return {
      Key_Input_Mode.CHANGE_VIEW:  Key_Input_Mode.CHANGE_SLIDE,
      Key_Input_Mode.CHANGE_SLIDE: Key_Input_Mode.CHANGE_VIEW,
    }[self]


class General_Key_Bindings:
  ## close window
  KEY_EXIT = 'escape'

  ## close window
  KEY_CHANGE_MODE = 'm'


class Change_View_Key_Bindings:
  KEYS_MOVE_CAMERA_LEFT      = ['a', 'left']
  KEYS_MOVE_CAMERA_RIGHT     = ['d', 'right']
  KEYS_MOVE_CAMERA_UP        = ['w', 'up']
  KEYS_MOVE_CAMERA_DOWN      = ['s', 'down']
  KEYS_ZOOM_IN               = ['+', '=']
  KEYS_ZOOM_OUT              = ['-', '_']
  KEY_CAMERA_PROJECT_1ST_DIM = '1'
  KEY_CAMERA_PROJECT_2ND_DIM = '2'
  KEY_CAMERA_PROJECT_3RD_DIM = '3'
  KEY_RESET_CAMERA           = 'r'

  KEY_ORTHOGRAPHIC_PROJECTION = 'o'
  KEY_PERSPECTIVE_PROJECTION  = 'p'


class Change_Slide_Key_Bindings:
  KEYS_GOTO_PREVIOUS_STEP    = ['a', 'left']
  KEYS_GOTO_NEXT_STEP        = ['d', 'right']
  KEY_GO_BACKWARD_10_STEPS   = ['w', 'up']
  KEY_GO_FORWARD_10_STEPS    = ['s', 'down']
  KEY_GOTO_FIRST_STEP        = 'pageup'
  KEY_GOTO_LAST_STEP         = 'pagedown'
  KEY_GOTO_STEP_IN_KEYBUFFER = 'enter'

  @staticmethod
  def key_is_digit(key):
    return key in '0123456789'


def randrange(n, vmin, vmax):
    '''
    Helper function to make an array of random numbers having shape (n, )
    with each number distributed Uniform(vmin, vmax).
    '''
    return (vmax - vmin) * np.random.rand(n) + vmin


def plot_document(doc: Document, name: str = ''):
  assert type(doc) is Document, doc

  plt.rcParams['toolbar'] = 'None'

  fig = plt.figure()

  if name:
    fig.canvas.set_window_title(name)

  if len(doc.images) == 1:
    mode = Key_Input_Mode.CHANGE_VIEW
  else:
    mode = Key_Input_Mode.CHANGE_SLIDE

  fig.text(0.05, 0.05, f'mode: {mode.value}', fontweight=500)

  image_idx  = 0
  azimuth    = 0
  elevation  = 0
  zoom       = 0
  projection = 'persp'
  key_buf    = ''

  DEFAULT_AZIMUTH   = -60
  DEFAULT_ELEVATION = +30

  AZIMUTH_SPEED   = 3
  ELEVATION_SPEED = 3
  ZOOM_SPEED      = 1.5

  def draw_image(image_idx: int):
    fig.clear()
    fig.text(0.05, 0.05, f'mode: {mode.value}', fontweight=750)
    if key_buf:
      fig.text(0.05, 0.10, f'Goto slide: {key_buf!r}', fontweight=750)
    plot_image(doc.images[image_idx], doc.min_coords, doc.max_coords,
               figure=fig, azimuth=azimuth, elevation=elevation, zoom=zoom, projection=projection)
    plt.draw()

  def on_mouse_click(event):
    nonlocal image_idx

    if event.button == 1:
      image_idx += 1
    elif event.button == 3:
      image_idx -= 1

    image_idx %= len(doc.images)
    draw_image(image_idx)

  def on_key_press(event):
    nonlocal mode, image_idx, fig, key_buf, azimuth, elevation, zoom, projection

    if event.key == General_Key_Bindings.KEY_EXIT:
      plt.close(event.canvas.figure)
      return
    elif event.key == General_Key_Bindings.KEY_CHANGE_MODE:
      mode = mode.next()
    elif mode is Key_Input_Mode.CHANGE_VIEW:
      if event.key in Change_View_Key_Bindings.KEYS_MOVE_CAMERA_RIGHT:
        azimuth += AZIMUTH_SPEED
        key_buf = ''
      elif event.key in Change_View_Key_Bindings.KEYS_MOVE_CAMERA_LEFT:
        azimuth -= AZIMUTH_SPEED
        key_buf = ''
      elif event.key in Change_View_Key_Bindings.KEYS_MOVE_CAMERA_UP:
        elevation += ELEVATION_SPEED
        key_buf = ''
      elif event.key in Change_View_Key_Bindings.KEYS_MOVE_CAMERA_DOWN:
        elevation -= ELEVATION_SPEED
        key_buf = ''
      elif event.key in Change_View_Key_Bindings.KEYS_ZOOM_IN:
        zoom -= ZOOM_SPEED
        key_buf = ''
      elif event.key in Change_View_Key_Bindings.KEYS_ZOOM_OUT:
        zoom += ZOOM_SPEED
        key_buf = ''
      elif event.key == Change_View_Key_Bindings.KEY_RESET_CAMERA:
        ## reset to standard view
        azimuth   = 0
        elevation = 0
        key_buf   = ''
      elif event.key == Change_View_Key_Bindings.KEY_CAMERA_PROJECT_1ST_DIM:
        # set viewpoint to project away the 1st dimension (k)
        azimuth   = -DEFAULT_AZIMUTH
        elevation = -DEFAULT_ELEVATION
        key_buf = ''
      elif event.key == Change_View_Key_Bindings.KEY_CAMERA_PROJECT_2ND_DIM:
        # set viewpoint to project away the 2nd dimension (j)
        azimuth   = 0.5 * DEFAULT_AZIMUTH
        elevation = -DEFAULT_ELEVATION
        key_buf = ''
      elif event.key == Change_View_Key_Bindings.KEY_CAMERA_PROJECT_3RD_DIM:
        # set viewpoint to project away the 3rd dimension (i)
        azimuth   = 0.5 * DEFAULT_AZIMUTH
        elevation = +2 * DEFAULT_ELEVATION
        key_buf = ''
      elif event.key == Change_View_Key_Bindings.KEY_ORTHOGRAPHIC_PROJECTION:
        ## reset view projection to orthographic projection
        projection = 'ortho'
        key_buf = ''
      elif event.key == Change_View_Key_Bindings.KEY_PERSPECTIVE_PROJECTION:
        ## reset view projection to perspective projection
        projection = 'persp'
        key_buf = ''
      else:
        print(event.key)
        return
    elif mode is Key_Input_Mode.CHANGE_SLIDE:
      if event.key in Change_Slide_Key_Bindings.KEYS_GOTO_PREVIOUS_STEP:
        image_idx -= 1
        key_buf = ''
      elif event.key in Change_Slide_Key_Bindings.KEYS_GOTO_NEXT_STEP:
        image_idx += 1
        key_buf = ''
      elif event.key in Change_Slide_Key_Bindings.KEY_GO_BACKWARD_10_STEPS:
        image_idx -= 10
        key_buf = ''
      elif event.key in Change_Slide_Key_Bindings.KEY_GO_FORWARD_10_STEPS:
        image_idx += 10
        key_buf = ''
      elif event.key == Change_Slide_Key_Bindings.KEY_GOTO_FIRST_STEP:
        image_idx = 0
        key_buf = ''
      elif event.key == Change_Slide_Key_Bindings.KEY_GOTO_LAST_STEP:
        image_idx = len(doc.images) - 1
        key_buf = ''
      elif Change_Slide_Key_Bindings.key_is_digit(event.key):
        key_buf += event.key
      elif event.key == Change_Slide_Key_Bindings.KEY_GOTO_STEP_IN_KEYBUFFER and key_buf:
        image_idx = int(key_buf)
        key_buf = ''
      else:
        # print(event.key)
        return

    image_idx %= len(doc.images)
    draw_image(image_idx)

  fig.canvas.mpl_connect('button_press_event', on_mouse_click)
  fig.canvas.mpl_connect('key_press_event', on_key_press)

  draw_image(0)
  plt.show()


def plot_image(image: Image,
               min_coords: Vector, max_coords: Vector,
               figure: plt.figure,
               elevation: float = 0, azimuth: float = 0, zoom: float = 0,
               projection: str = 'persp',):
    label = f'Step {image.count}:'

    if image.description:
      description = image.description.replace('--->', '$\\rightarrow$')
      description = image.description.replace('->', '$\\rightarrow$')
      label += ' ' + description

      figure.suptitle(label)

    assert 0 < image.dimension <= 3

    plot_in_3D = (image.dimension == 3)

    MARGIN = 0.3

    if plot_in_3D:
      ax = mpl_toolkits.mplot3d.Axes3D(figure)

      ax.set_proj_type(projection)

      ax.set_xlabel('k')
      ax.set_ylabel('j')
      ax.set_zlabel('i')

      ax.view_init(elev=ax.elev + elevation, azim=ax.azim + azimuth)
      ax.dist += zoom

      for poly in image.polys:
        plot_polyhedron_3d(poly.colour, poly.extreme_points, poly.label, poly.affine_functions, axis=ax)

      ax.set_xlim(min_coords[0] - MARGIN, max(1, max_coords[0]) + MARGIN)
      ax.set_ylim(min_coords[1] - MARGIN, max(1, max_coords[1]) + MARGIN)
      ax.set_zlim(min_coords[2] - MARGIN, max(1, max_coords[2]) + MARGIN)
    else:
      ax = plt.axes()

      ax.set_xlabel('k')
      ax.set_ylabel('j')

      for poly in image.polys:
        plot_polyhedron_2d(poly.colour, poly.extreme_points, poly.label, poly.affine_functions, axis=ax)

      ax.set_xlim(min_coords[0] - MARGIN, max(1, max_coords[0]) + MARGIN)
      ax.set_ylim(min_coords[1] - MARGIN, max(1, max_coords[1]) + MARGIN)


def plot_polyhedron_2d(colour: Colour, extreme_points: List[Vector],
                       label = None, affine_functions = [], *, axis: plt.Axes):
  colour     = colour.matplotlib
  num_points = len(extreme_points)

  # Fabian, what, we can't plot 4D plots ??????
  # Sorry Manu, I'll implement that next weekend.
  assert num_points in [1, 2, 4, 8], f'polyhedron has unsupported number of points: {extreme_points}'

  def coordinates(v: Vector) -> np.array:
    assert len(v) < 4

    v = np.array(v)

    while len(v) < 2:
      v = np.hstack([v, [0]])

    return v[0:2]

  def plot_label(label, affine_functions):
    if label:
      center = list(sum(extreme_points[1:], extreme_points[0]) / len(extreme_points))

      if num_points > 4:
        # If we are in 3D we plot the label `atop` the poly.
        # Oherwise it often gets obscured by lines.
        center[-1] = max(p[-1] for p in extreme_points)
      elif num_points == 2:
        # Plot the text slightly above the line
        center[1] += 0.015
      elif num_points == 1:
        # Plot the text slightly above the point
        center[1] += 0.025

      # axis.text(x=center[0], y=center[1], s=label)

      for i, fn in enumerate(affine_functions):
        variables = [None, 'k', 'j', 'i']

        coeff_strs = [str(fn.coeffs[0])]
        for dim in range(1, fn.num_dimensions + 1):
          ct = fn.coeff_types[dim]
          if ct.value == 0:  # UNCHANGED
            coeff_str = ('$\\bot_{' + str(fn.coeffs[dim]) + '}')
          elif ct.value == 2:  # WIDENED
            coeff_str = ('$\\top ')
          elif ct.value == 1:  # DETERMINED
            coeff_str = '$' + str(fn.coeffs[dim])
          else:
            assert False, 'Should not be there, ct=' + str(ct)
          coeff_str = coeff_str + variables[dim] + '$'
          coeff_strs.append(coeff_str)

        label = '+'.join(reversed(coeff_strs))

    axis.text(x=center[0], y=center[1], s=label)

  def plot_point(point):
    axis.scatter(x=[point[0]], y=[point[1]], c=colour)

  def plot_line(start, end):
    start = coordinates(start)
    end   = coordinates(end)

    lines = matplotlib.collections.LineCollection([[start, end]])
    lines.set_edgecolor(colour)
    axis.add_collection(lines)

  def plot_rectangle(lower_left, lower_right, upper_left, upper_right):
    lower_left  = coordinates(lower_left)
    lower_right = coordinates(lower_right)
    upper_left  = coordinates(upper_left)
    upper_right = coordinates(upper_right)

    lines = matplotlib.collections.LineCollection([
      [lower_left, lower_right],
      [lower_right, upper_right],
      [upper_right, upper_left],
      [upper_left, lower_left],
    ])
    lines.set_edgecolor(colour)
    axis.add_collection(lines)

  if num_points == 1:
    plot_point(*extreme_points)
  elif num_points == 2:
    plot_line(*extreme_points)
  elif num_points == 4:
    plot_rectangle(*extreme_points)
  else:
    raise ValueError('impossible 2D polyhedron: ' + str(extreme_points))

  plot_label(label, affine_functions)


def plot_polyhedron_3d(colour: Colour, extreme_points: List[Vector],
                       label = None, affine_functions = [], *, axis: mpl_toolkits.mplot3d.Axes3D):
  colour     = colour.latex
  num_points = len(extreme_points)

  # Fabian, what, we can't plot 4D plots ??????
  # Sorry Manu, I'll implement that next weekend.
  assert num_points in [1, 2, 4, 8], f'polyhedron has unsupported number of points: {extreme_points}'

  def coordinates(v: Vector) -> np.array:
    assert len(v) < 4

    v = np.array(v)

    while len(v) < 3:
      v = np.hstack([v, [0]])

    return v[0:3]

  def plot_label(label, affine_functions):
    if label:
      center = list(sum(extreme_points[1:], extreme_points[0]) / len(extreme_points))

      if num_points > 4:
        # If we are in 3D we plot the label `atop` the poly.
        # Oherwise it often gets obscured by lines.
        center[-1] = max(p[-1] for p in extreme_points)
      elif num_points == 2:
        # Plot the text slightly above the line
        center[1] += 0.015
      elif num_points == 1:
        # Plot the text slightly above the point
        center[1] += 0.025

      # axis.text(x=center[0], y=center[1], s=label)

      for i, fn in enumerate(affine_functions):
        variables = [None, 'k', 'j', 'i']

        coeff_strs = [str(fn.coeffs[0])]
        for dim in range(1, fn.num_dimensions + 1):
          ct = fn.coeff_types[dim]
          if ct.value == 0:  # UNCHANGED
            coeff_str = ('$\\bot_{' + str(fn.coeffs[dim]) + '}')
          elif ct.value == 2:  # WIDENED
            coeff_str = ('$\\top ')
          elif ct.value == 1:  # DETERMINED
            coeff_str = '$' + str(fn.coeffs[dim])
          else:
            assert False, 'Should not be there, ct=' + str(ct)
          coeff_str = coeff_str + variables[dim] + '$'
          coeff_strs.append(coeff_str)

        label = '+'.join(reversed(coeff_strs))

    axis.text(x=center[0], y=center[1], z=center[2], s=label)

  def plot_point(point):
    axis.scatter(xs=[point[0]], ys=[point[1]], zs=[point[2]], zdir='z', c='r')

  def plot_line(start, end):
    start = coordinates(start)
    end   = coordinates(end)

    lines = mpl_toolkits.mplot3d.art3d.Line3DCollection([[start, end]])
    lines.set_edgecolor('k')
    axis.add_collection3d(lines)

  def plot_rectangle(lower_left, lower_right, upper_left, upper_right):
    lower_left  = coordinates(lower_left)
    lower_right = coordinates(lower_right)
    upper_left  = coordinates(upper_left)
    upper_right = coordinates(upper_right)

    lines = mpl_toolkits.mplot3d.art3d.Line3DCollection([
      [lower_left, lower_right],
      [lower_right, upper_right],
      [upper_right, upper_left],
      [upper_left, lower_left],
    ])
    lines.set_edgecolor(colour)
    axis.add_collection(lines)

  def plot_cube(extreme_points):
    # bottom rectangle
    bot_lower_left  = coordinates(extreme_points[0])
    bot_lower_right = coordinates(extreme_points[1])
    bot_upper_left  = coordinates(extreme_points[2])
    bot_upper_right = coordinates(extreme_points[3])
    # top rectangle
    top_lower_left  = coordinates(extreme_points[4])
    top_lower_right = coordinates(extreme_points[5])
    top_upper_left  = coordinates(extreme_points[6])
    top_upper_right = coordinates(extreme_points[7])

    lines = mpl_toolkits.mplot3d.art3d.Line3DCollection([
      # bottom rectangle
      [bot_lower_left,  bot_lower_right],
      [bot_lower_right, bot_upper_right],
      [bot_upper_right, bot_upper_left],
      [bot_upper_left,  bot_lower_left],
      # top rectangle
      [top_lower_left,  top_lower_right],
      [top_lower_right, top_upper_right],
      [top_upper_right, top_upper_left],
      [top_upper_left,  top_lower_left],
      # connect the two rectangles
      [bot_lower_left,  top_lower_left],
      [bot_lower_right, top_lower_right],
      [bot_upper_left,  top_upper_left],
      [bot_upper_right, top_upper_right],
    ])
    lines.set_edgecolor(colour)
    axis.add_collection(lines)

  if num_points == 1:
    plot_point(*extreme_points)
  elif num_points == 2:
    plot_line(*extreme_points)
  elif num_points == 4:
    plot_rectangle(*extreme_points)
  elif num_points == 8:
    plot_cube(extreme_points)
    # # bottom rectangle
    # plot_rectangle(*extreme_points[0:4])
    # # top rectangle
    # plot_rectangle(*extreme_points[4:8])
    # # connect the two rectangles
    # plot_line(extreme_points[0], extreme_points[4])
    # plot_line(extreme_points[1], extreme_points[5])
    # plot_line(extreme_points[2], extreme_points[6])
    # plot_line(extreme_points[3], extreme_points[7])
  else:
    raise ValueError('impossible 3D polyhedron: ' + str(extreme_points))

  plot_label(label, affine_functions)
