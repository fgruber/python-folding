## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

from folding.NamedStruct import NamedStruct
from folding.Vector      import Vector
from typing import *
import enum
import sys


def old_eats_new():
  return document(
    images=[
      image(
        'first point',
        new_point([0, 0]),
      ),
      image(
        'first point becomes current line',
        current_line([0, 0], [0, 0]),
        changed_dimensions=[0],
      ),
      image(
        'new point',
        current_line([0, 0], [0, 0]),
        new_point([0, 1]),
        changed_dimensions=[0],
      ),
      image(
        'extend current line',
        current_line([0, 0], [0, 1]),
        changed_dimensions=[0],
      ),
      image(
        'new point',
        current_line([0, 0], [0, 1]),
        new_point([0, 3]),
        changed_dimensions=[0],
      ),
      image(
        'could not merge new point to current line. promote: cl -> old/1. new point becomes cl',
        old_poly(1, (), [0, 0], [0, 1]),
        current_line([0, 3]),
        changed_dimensions=[0],
      ),
      image(
        'new point',
        old_poly(1, (), [0, 0], [0, 1]),
        current_line([0, 3]),
        new_point([0, 4]),
        changed_dimensions=[0],
      ),
      image(
        'extend current line',
        old_poly(1, (), [0, 0], [0, 1]),
        current_line([0, 3], [0, 4]),
        changed_dimensions=[0],
      ),
      image(
        'new point and inner loop finished',
        old_poly(1, (), [0, 0], [0, 1]),
        current_line([0, 3], [0, 4]),
        new_point([1, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'promote 1->2: current-line -> old/1',
        old_poly(1, (), [0, 0], [0, 1]),
        old_poly(1, (), [0, 3], [0, 4]),
        new_point([1, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'promote 1->2: old/1 -> new/2',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        new_point([1, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'old absorb new: nothing to do',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        new_point([1, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'new point becomes current line',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        current_line([1, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'new point',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        current_line([1, 0]),
        new_point([1, 1]),
        changed_dimensions=[0],
      ),
      image(
        'extend current line',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        current_line([1, 0], [1, 1]),
        changed_dimensions=[0],
      ),
      image(
        'new point',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        current_line([1, 0], [1, 1]),
        new_point([1, 3]),
        changed_dimensions=[0],
      ),
      image(
        'could not merge new point to current line. promote: cl -> old/1. new point becomes cl',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        current_line([1, 3]),
        changed_dimensions=[0],
      ),
      image(
        'new point',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        current_line([1, 3]),
        new_point([1, 4]),
        changed_dimensions=[0],
      ),
      image(
        'extend current line',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        current_line([1, 3], [1, 4]),
        changed_dimensions=[0],
      ),
      image(
        'new point and inner loop finished',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        current_line([1, 3], [1, 4]),
        new_point([2, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'promote 1->2: current-line -> old/1',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        old_poly(1, (), [1, 3], [1, 4]),
        new_point([2, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'promote 1->2: new/2 -> old/2',
        old_poly(2, (), [0, 0], [0, 1]),
        old_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        old_poly(1, (), [1, 3], [1, 4]),
        new_point([2, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'promote 1->2: old/1 -> new/2',
        old_poly(2, (), [0, 0], [0, 1]),
        old_poly(2, (), [0, 3], [0, 4]),
        new_poly(2, (), [1, 0], [1, 1]),
        new_poly(2, (), [1, 3], [1, 4]),
        new_point([2, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        'absorb',
        old_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        old_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        new_point([2, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        current_line([2, 0]),
        changed_dimensions=[1, 0],
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        current_line([2, 0]),
        new_point([2, 1]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        current_line([2, 0], [2, 1]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        current_line([2, 0], [2, 1]),
        new_point([2, 3]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        old_poly(1, (), [2, 0], [2, 1]),
        current_line([2, 3]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        old_poly(1, (), [2, 0], [2, 1]),
        current_line([2, 3]),
        new_point([2, 4]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        old_poly(1, (), [2, 0], [2, 1]),
        current_line([2, 3], [2, 4]),
      ),
      image(
        'outermost loop iterated',
        new_poly(2, (), [0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1]),
        new_poly(2, (), [0, 0, 3], [0, 0, 4], [0, 1, 3], [0, 1, 4]),
        old_poly(1, (), [0, 2, 0], [0, 2, 1]),
        current_line([0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        'promote',
        new_poly(2, (), [0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1]),
        new_poly(2, (), [0, 0, 3], [0, 0, 4], [0, 1, 3], [0, 1, 4]),
        old_poly(1, (), [0, 2, 0], [0, 2, 1]),
        old_poly(1, (), [0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        'promote',
        old_poly(2, (), [0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1]),
        old_poly(2, (), [0, 0, 3], [0, 0, 4], [0, 1, 3], [0, 1, 4]),
        new_poly(1, (), [0, 2, 0], [0, 2, 1]),
        new_poly(1, (), [0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        'absorb',
        old_poly(2, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        old_poly(2, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        'promote',
        new_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        new_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        '...',
        new_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        new_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        old_poly(2, (), [1, 0, 0], [1, 0, 1], [1, 2, 0], [1, 2, 1]),
        old_poly(2, (), [1, 0, 3], [1, 0, 4], [1, 2, 3], [1, 2, 4]),
      ),
      image(
        'promote',
        old_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        old_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        old_poly(2, (), [1, 0, 0], [1, 0, 1], [1, 2, 0], [1, 2, 1]),
        old_poly(2, (), [1, 0, 3], [1, 0, 4], [1, 2, 3], [1, 2, 4]),
      ),
      image(
        'promote (!?! when should this happen !?!)',
        old_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        old_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        new_poly(2, (), [1, 0, 0], [1, 0, 1], [1, 2, 0], [1, 2, 1]),
        new_poly(2, (), [1, 0, 3], [1, 0, 4], [1, 2, 3], [1, 2, 4]),
      ),
      image(
        'absorb',
        old_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1], [1, 0, 0], [1, 0, 1], [1, 2, 0], [1, 2, 1]),
        old_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4], [1, 0, 3], [1, 0, 4], [1, 2, 3], [1, 2, 4]),
      ),
    ],
  )


def new_eats_old():
  return document(
    images=[
      image(
        'first point',
        new_point([0, 0]),
      ),
      image(
        'first point becomes current line',
        current_line([0, 0])
      ),
      image(
        'new point',
        current_line([0, 0]),
        new_point([0, 1]),
      ),
      image(
        'extend current line',
        current_line([0, 0], [0, 1])
      ),
      image(
        'new point',
        current_line([0, 0], [0, 1]),
        new_point([0, 3]),
      ),
      image(
        'could not merge new point to current line. promote: cl -> new/1. new point becomes cl',
        new_poly(1, (), [0, 0], [0, 1]),
        current_line([0, 3]),
      ),
      image(
        'new point',
        new_poly(1, (), [0, 0], [0, 1]),
        current_line([0, 3]),
        new_point([0, 4]),
      ),
      image(
        'extend current line',
        new_poly(1, (), [0, 0], [0, 1]),
        current_line([0, 3], [0, 4]),
      ),
      image(
        'new point and inner loop finished',
        new_poly(1, (), [0, 0], [0, 1]),
        current_line([0, 3], [0, 4]),
        new_point([1, 0]),
      ),
      image(
        'promote 1->2: current-line -> old/1',
        new_poly(1, (), [0, 0], [0, 1]),
        new_poly(1, (), [0, 3], [0, 4]),
        new_point([1, 0]),
      ),
      image(
        'promote 1->2: old/1 -> new/2',
        new_poly(1, (), [0, 0], [0, 1]),
        new_poly(1, (), [0, 3], [0, 4]),
        new_point([1, 0]),
      ),
      image(
        'old absorb new: nothing to do',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        new_point([1, 0]),
      ),
      image(
        'new point becomes current line',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        current_line([1, 0]),
      ),
      image(
        'new point',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        current_line([1, 0]),
        new_point([1, 1]),
      ),
      image(
        'extend current line',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        current_line([1, 0], [1, 1]),
      ),
      image(
        'new point',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        current_line([1, 0], [1, 1]),
        new_point([1, 3]),
      ),
      image(
        'could not merge new point to current line. promote: cl -> old/1. new point becomes cl',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        current_line([1, 3]),
      ),
      image(
        'new point',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        current_line([1, 3]),
        new_point([1, 4]),
      ),
      image(
        'extend current line',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        current_line([1, 3], [1, 4]),
      ),
      image(
        'new point and inner loop finished',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        current_line([1, 3], [1, 4]),
        new_point([2, 0]),
      ),
      image(
        'promote 1->2: current-line -> old/1',
        new_poly(2, (), [0, 0], [0, 1]),
        new_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        old_poly(1, (), [1, 3], [1, 4]),
        new_point([2, 0]),
      ),
       image(
        'promote 1->2: new/2 -> old/2',
        old_poly(2, (), [0, 0], [0, 1]),
        old_poly(2, (), [0, 3], [0, 4]),
        old_poly(1, (), [1, 0], [1, 1]),
        old_poly(1, (), [1, 3], [1, 4]),
        new_point([2, 0]),
      ),
      image(
        'promote 1->2: old/1 -> new/2',
        old_poly(2, (), [0, 0], [0, 1]),
        old_poly(2, (), [0, 3], [0, 4]),
        new_poly(2, (), [1, 0], [1, 1]),
        new_poly(2, (), [1, 3], [1, 4]),
        new_point([2, 0]),
      ),
      image(
        'absorb',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        new_point([2, 0]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        current_line([2, 0]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        current_line([2, 0]),
        new_point([2, 1]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        current_line([2, 0], [2, 1]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        current_line([2, 0], [2, 1]),
        new_point([2, 3]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        old_poly(1, (), [2, 0], [2, 1]),
        current_line([2, 3]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        old_poly(1, (), [2, 0], [2, 1]),
        current_line([2, 3]),
        new_point([2, 4]),
      ),
      image(
        '',
        new_poly(2, (), [0, 0], [0, 1], [1, 0], [1, 1]),
        new_poly(2, (), [0, 3], [0, 4], [1, 3], [1, 4]),
        old_poly(1, (), [2, 0], [2, 1]),
        current_line([2, 3], [2, 4]),
      ),
      image(
        'outermost loop iterated',
        new_poly(2, (), [0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1]),
        new_poly(2, (), [0, 0, 3], [0, 0, 4], [0, 1, 3], [0, 1, 4]),
        old_poly(1, (), [0, 2, 0], [0, 2, 1]),
        current_line([0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        'promote',
        new_poly(2, (), [0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1]),
        new_poly(2, (), [0, 0, 3], [0, 0, 4], [0, 1, 3], [0, 1, 4]),
        old_poly(1, (), [0, 2, 0], [0, 2, 1]),
        old_poly(1, (), [0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        'promote',
        old_poly(2, (), [0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1]),
        old_poly(2, (), [0, 0, 3], [0, 0, 4], [0, 1, 3], [0, 1, 4]),
        new_poly(1, (), [0, 2, 0], [0, 2, 1]),
        new_poly(1, (), [0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        'absorb',
        old_poly(2, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        old_poly(2, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        'promote',
        new_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        new_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        new_point([1, 0, 0]),
      ),
      image(
        '...',
        new_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        new_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        old_poly(2, (), [1, 0, 0], [1, 0, 1], [1, 2, 0], [1, 2, 1]),
        old_poly(2, (), [1, 0, 3], [1, 0, 4], [1, 2, 3], [1, 2, 4]),
      ),
      image(
        'promote',
        old_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        old_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        old_poly(2, (), [1, 0, 0], [1, 0, 1], [1, 2, 0], [1, 2, 1]),
        old_poly(2, (), [1, 0, 3], [1, 0, 4], [1, 2, 3], [1, 2, 4]),
      ),
      image(
        'promote (!?! when should this happen !?!)',
        old_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1]),
        old_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4]),
        new_poly(2, (), [1, 0, 0], [1, 0, 1], [1, 2, 0], [1, 2, 1]),
        new_poly(2, (), [1, 0, 3], [1, 0, 4], [1, 2, 3], [1, 2, 4]),
      ),
      image(
        'absorb',
        old_poly(3, (), [0, 0, 0], [0, 0, 1], [0, 2, 0], [0, 2, 1], [1, 0, 0], [1, 0, 1], [1, 2, 0], [1, 2, 1]),
        old_poly(3, (), [0, 0, 3], [0, 0, 4], [0, 2, 3], [0, 2, 4], [1, 0, 3], [1, 0, 4], [1, 2, 3], [1, 2, 4]),
      ),
    ],
  )


################################################################################
### HIGH(-ISH) LEVEL PRINTING API

def document(*, images):
  num_dims   = max([i.dimension for i in images], default=0)
  min_coords = [+0] * num_dims
  max_coords = [+1] * num_dims

  for image in images:
    for poly in image.polys:
      for point in poly.extreme_points:
        for i in range(point.dimension):
          max_coords[i] = max(max_coords[i], point[i])
          min_coords[i] = min(min_coords[i], point[i])

  min_coords = Vector(min_coords)
  max_coords = Vector(max_coords)

  return Document(num_dims, min_coords, max_coords, images)


def image(description, *polys, changed_dimensions=()):
  return Image(description, polys, changed_dimensions)


def new_point(coordinates):
  pos = Vector(coordinates)
  return Print_Poly(Colour.BLUE, [pos], '$\\bullet$')


def current_line(start, end = None):
  if end is None:
    end = start

  points = [Vector(start), Vector(end)]
  return Print_Poly(Colour.RED, points)


def old_poly(dimension,  affine_functions, *points):
  points = [Vector(p) for p in points]
  return Print_Poly(Colour.BLACK, points, str(dimension), affine_functions)


def new_poly(dimension, affine_functions, *points):
  points = [Vector(p) for p in points]
  return Print_Poly(Colour.GREEN, points, str(dimension), affine_functions)


def retired_poly(dimension,  affine_functions, *points):
  points = [Vector(p) for p in points]
  return Print_Poly(Colour.GREY, points, str(dimension), affine_functions)


### AST FOR HIGH LEVEL API

class Document:
  def __init__(self, num_dimensions: int, min_coords: Vector, max_coords: Vector, images: List['Image']):
    assert num_dimensions == len(max_coords)

    self.num_dimensions = num_dimensions
    self.min_coords     = min_coords
    self.max_coords     = max_coords
    self.images         = list(images)

  def add_image(self, image):
    self.images.append(image)

  def print_latex(self, file=sys.stdout):
    print(TEMPLATE_START_DOCUMENT, file=file)
    for i in self.images:
      i.print_latex(self.max_coords, file=file)
    print(TEMPLATE_END_DOCUMENT, file=file)


class Image:
  def __init__(self, description, polys, changed_dimensions):
    self.description        = description
    self.polys              = list(polys)
    self.changed_dimensions = changed_dimensions
    self.count              = Image._COUNT_

    Image._COUNT_ += 1

  def add_poly(self, poly):
    self.polys.append(poly)

  _COUNT_ = 0

  @property
  def dimension(self):
    if self.polys:
      return max(2, max(p.dimension for p in self.polys))
    else:
      return 2


class Print_Poly(NamedStruct):
  colour:           'Colour'
  extreme_points:   List[Vector]
  label:            Optional[str]
  affine_functions: list  # List[AffineFunction], but folder.AffineFunction is not declared here

  def __post_init__(self):
    P = self.extreme_points

    assert len(P) == 1 or len(P) % 2 == 0, P

    # check if poly is degenerate (& remove fake dimension if so)
    while len(P) > 1 and all(P[i] == P[i + len(P) // 2] for i in range(len(P) // 2)):
      # is degenerate
      P = P[0:len(P) // 2]

    self.extreme_points = P

    assert len(P) in [1, 2, 4, 8], f'poly cannot be displayed in 3D ({P})'

  @property
  def dimension(self):
    return max(p.dimension for p in self.extreme_points)


class Colour(enum.Enum):
  BLACK = 'black', 'k'
  RED   = 'red',   'r'
  GREEN = 'Green', 'g'
  BLUE  = 'blue',  'b'
  GREY  = 'gray',  '#282828'

  @property
  def latex(self):
    return self.value[0]

  @property
  def matplotlib(self):
    return self.value[1]
