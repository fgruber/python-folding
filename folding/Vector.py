## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

__all__ = [
  'Vector',
]

import itertools
import operator
from typing import *


class Vector:
    """
        An immutable fixed-size vector of integers.
        Used for point coordinates & labels
    """

    __slots__ = ('_vs', )

    def __init__(self, values: Iterable[int]) -> 'Vector':
        self._vs = tuple(values)
        assert all(type(n) is int for n in self), self._vs

    def __len__(self) -> int:
        return len(self._vs)

    def __iter__(self) -> Iterator[int]:
        return iter(self._vs)

    def __reversed__(self) -> Iterator[int]:
        return reversed(self._vs)

    def __getitem__(self, idx: int) -> int:
        try:
            return self._vs[idx]
        except IndexError:
            raise IndexError(f'Vector index out of range (index is {idx}, len is {len(self)})')

    def __eq__(self, that) -> bool:
        return type(self) is type(that) and self._vs == that._vs

    def __hash__(self) -> int:
        return hash(self._vs)

    @property
    def dimension(self):
        return len(self._vs)

    def is_a_search_vector(self, d: int) -> bool:
        """
            Check if this vector is a valid search vector in a d dimensional space.

            A search vector V is defined as:
                V[i] if i  < d-1 in {-1, 0, +1}
                V[i] if i == d-1 in {1}
                V[i] if i  > d-1 in {0}
        """

        assert 0 < d <= len(self), f'0 < {d} <= {len(self)}'

        for i in range(d - 1):
            if self[i] < -1 or self[i] > +1:
                return False

        if self[d - 1] != 1:
            return False

        for i in range(d, len(self)):
            if self[i] != 0:
                return False

        return True

    @staticmethod
    def make_search_vectors(MaxDim: int, d: int) -> Tuple[Tuple['Vector', ...], ...]:
        """
            Create search vectors for a :d: dimensional space
            embedded in a :MaxDim: dimensional space.

            0 < :d: <= :MaxDim:

            A search vector V is defined as a vector with MaxDim components where:
                V[i] if i  < d in {-1, 0, +1}
                V[i] if i == d in {1}
                V[i] if i  > d in {0}

        """

        if d == 0:
            return None

        assert 0 < d <= MaxDim, d

        zeroes = (0,) * (MaxDim - d)

        Vs = ()

        for direction in itertools.product([-1, 0, +1], repeat=d - 1):
            v = Vector(direction + (1,) + zeroes)

            assert len(v) == MaxDim, [direction, (1,), zeroes, v, MaxDim]
            assert v.is_a_search_vector(d)

            Vs += (v,)

        return Vs

    def __add__(self, that: Union['Vector', int]) -> 'Vector':
        """
            if :that: is a vector: create new Vector by adding elements of self and that pairwise
            if :that: is an int: create new Vector by adding that to all elements of self.
        """

        return self._binop(that, Vector, operator.add)

    def __sub__(self, that: Union['Vector', int]):
        """
          Same as __add__, but substracts instead of adding.
        """

        return self._binop(that, Vector, operator.sub)

    def __mul__(self, that: Union['Vector', int]) -> 'Vector':
        """
          Same as __add__, but multiplies instead of adding.
        """
        return self._binop(that, Vector, operator.mul)

    def __floordiv__(self, that: Union['Vector', int]) -> 'Vector':
        """
          Same as __add__, but uses // instead of +.
        """

        return self._binop(that, Vector, operator.floordiv)

    def __truediv__(self, that: Union['Vector', int]) -> Tuple[float, ...]:
        """
          Same as __add__, but uses / instead of +.
          Also returns a tuple of floats instead of a Vector since int/int returns float
          and we restrict Vectors to only contain ints.
        """

        return self._binop(that, tuple, operator.truediv)

    def append(self, n: int) -> 'Vector':
        """
            Create new vector that is a copy of self with one element n appended.
        """
        return Vector(itertools.chain(self, [n]))

    def shift(self, *offsets: Tuple[float, ...]) -> Tuple[float, ...]:
        """
            Create tuple of floats that is a copy of this Vector
            where all coordinates where offset by values in :offsets:.

            Vector(1,2,3).shift()                  -> (1.0,2.0,3.0)
            Vector(1,2,3).shift(0.3)               -> (1.3,2.0,3.0)
            Vector(1,2,3).shift(0.3, 0.2)          -> (1.3,2.2,3.1)
            Vector(1,2,3).shift(0.3, 0.2, 0.1)     -> (1.3,2.2,3.1)
            Vector(1,2,3).shift(0.3, 0.2, 0.1, 42) -> (1.3,2.2,3.1)
        """

        return tuple(map(lambda x: x[0] + x[1], itertools.zip_longest(self, offsets, fillvalue=0)))

    def _binop(self, that: Union['Vector', int], ctor, op) -> Tuple[float, ...]:
        if type(that) is type(self):
            assert len(self) == len(that)

            obj = ctor(op(self[i], that[i]) for i in range(len(self)))
            assert type(obj) is ctor
            return obj

        if type(that) is int:
            obj = ctor(op(n, that) for n in self)
            assert type(obj) is ctor
            return obj

        return NotImplemented

    def __str__(self):
        return str(self._vs)

    def __repr__(self):
        return type(self).__name__ + '([' + ', '.join(map(str, self._vs)) + '])'
