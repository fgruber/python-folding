#!/usr/bin/env python3
## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

import argparse
import copy
import enum
import itertools
import math
import re
import sys
from typing    import *
from typing.io import *


from folding.Affine_Function import Affine_Function
from folding.Point           import Point
from folding.Polyhedron      import Polyhedron
from folding.Vector          import Vector
from folding.Worklist        import Worklist

import folding.images            as images
import folding.beamer_images     as beamer
import folding.matplotlib_images as matplot

Dimension = int


################################################################################
##### CORE FOLDING ALGORITHM

## name of stream we are currently folding
context: str

## state of the folding algorithm (changes over time)

# index of maximum (outermost) dimension in this space
MaxDim: int

# list of list of search vectors per dimension
Vq: List[List[Vector]]

# list polyhedra to merge per dimension.
#   worklist_old[i] contains i-dimensional polyhedra.
# i.e.
#   worklist_old[0] always contains points
#   worklist_old[1] always contains lines
#   worklist_old[2] always contains planes
#   ...
# Same for worklist_new
#
# Polyhedra in worklist_old[i] can absorb polyhedra from worklist_new[i]
worklist_old: List[Worklist]
worklist_new: List[Worklist]

# map used to find candidates for merging.
# maps P[0] -> P
Poly: List[Dict[Vector, Polyhedron]]

# number of polyhedra in all worklists
num_live_polys: int

# list of retired polyhedra
retired: List[Polyhedron]

## global folding parameters (constant)

# apply widening (forced label merge) on label functions
widen_affine_functions: bool


class Logging:
    """
        logging & latex diagram printing state
    """

    # log state of folding at every step
    verbose: bool

    # whether to track the state of folding at every step
    track_state: bool

    # current point in stream (only used for emitting latex diagrams)
    point: Optional['Point']

    # dimension that is currently being iterated
    dim: int

    # list of frames stored for latex rendering
    images: List

    # maximum coordinate seem for each axis
    max_coords: Optional[Vector]


def init_fold(context_, MaxDim_, widen_affine_functions_: bool, verbose: bool, track_state: bool):
    global context, MaxDim, Vq, worklist_old, worklist_new, Poly, retired, num_live_polys
    global widen_affine_functions

    context = context_

    widen_affine_functions = widen_affine_functions_

    MaxDim  = MaxDim_
    NumDims = MaxDim + 1  # NOTE: zero is a dimension too

    Vq = tuple(Vector.make_search_vectors(MaxDim, d) for d in range(NumDims))
    Poly = [{} for _ in range(NumDims + 2)]
    worklist_old = [Worklist() for _ in range(NumDims + 1)]
    worklist_new = [Worklist() for _ in range(NumDims + 1)]

    num_live_polys = 0

    retired = []

    # latex printing state

    Logging.verbose     = verbose
    Logging.images      = []
    Logging.point       = None
    Logging.dim         = 0
    Logging.max_coords  = Vector([1 for _ in range(MaxDim_)])
    Logging.track_state = track_state


def fold(MaxDim: int, addDims: bool, threshold: int, events: Iterable['Event']):
    NumDims = MaxDim + 1

    global Vq, worklist_old, worklist_new, Poly, num_live_polys, retired

    print_state('init')

    for event in events:
        assert event.type in ('point', 'iter')

        if event.type == 'point':
            # set global variable `point` so `print_state()` can include include it in the tex slides.
            # `point` is *NOT* actually used in the folding algorithm, so we set it to None immediately afterwards.
            Logging.point = event.point
            print_state('new point:', repr(event.point))
            Logging.point = None

            num_live_polys += 1

            if num_live_polys > threshold:
                retire_early()
                print_state('retire early')

            assert num_live_polys <= threshold

            poly = Polyhedron.from_point(event.point, widen_affine_functions)
            worklist_old[0].push(poly)

        if event.type == 'iter':
            # dimension of loop that was iterated
            d = Logging.dim = event.dim

            # we allow NumDims + 1 because we flush all worklists
            # after having treated all points by iterating a fictional loop
            # outside our outermost loop
            assert 0 < d <= NumDims + 1, [0, d, NumDims + 1]

            print_state('BGN iter loop dim', d)

            # Add dimensions to handle more cases
            while addDims and len(worklist[q]) > threshold and worklist[q].addDim(x):
                pass

            # Geometric widening
            widen_geom = None  # FIXME add parameter
            if widen_geom and len(worklist[q]) > threshold:
                worklist[q].widenPoly(x)

            # 1.   promote: old/d-1 -> new/d
            # 2.1. absorb:  old/d absorbs new/d
            # 2.2. all old/x that did not absorb anything -> new/d+1
            # 3.   promote: new/d -> old/d

            print_state(f'BGN 1: promote old/{d-1} -> new/{d}')
            for P in worklist_old[d - 1]:
                promote_old_to_new(d - 1, d, P)
            print_state(f'END 1: promote old/{d-1} -> new/{d}')

            print_state(f'BGN 2: old/{d} absorbs new/{d}')
            for P1 in worklist_old[d]:
                grew = False

                # 2.1:
                log('#', 'Poly', Poly[d])

                if P1.is_degenerate:
                    search_vectors = Vq[d]
                else:
                    search_vectors = (P1.growing_directions[0],)

                for v in search_vectors:
                    assert type(v) is Vector, v
                    assert type(P1[0]) is Vector

                    log('#>>>', P1, 'looking for', P1[0], '+', v, '->', P1[0] + v)

                    P2 = Poly[d].get(P1[0] + v)

                    if not P2:
                        continue

                    log('#>>>', P1, 'tries geom. absorb', P2)

                    # geometric criterion
                    if P1.canExtend(P2):
                        log('#>>>', P1, 'tries label absorb', P2)

                        A1 = P1.affine_functions
                        A2 = P2.affine_functions

                        # label criterion
                        if all(A1[i].canBeExtendedWith(A2[i], d) for i in range(len(A1))):
                            # extend P1 to include P2
                            log('#>>>', P1, 'absorbs', P2, end=' ')
                            P1.extend(P2, d)
                            worklist_new[d].remove(P2)
                            Poly[d].pop(P2[0])
                            # extend affine functions of P1 with ones of P2
                            for i in range(len(A1)):
                                A1[i].extendWith(A2[i], d)
                            num_live_polys -= 1
                            log('becomes', P1)
                            grew = True

                # 2.2:
                if not grew:
                    log('#>>>', P1, 'did not absorb anything')
                    promote_old_to_new(d, d + 1, P1)
            print_state(f'END 2: old/{d} absorbs new/{d}')

            # 3:
            print_state(f'BGN 3: promote new/{d} -> old/{d}')
            for P in worklist_new[d]:
                promote_new_to_old(d, d, P)
            print_state('END 3:')

            print_state('END iter loop dim', d)

    print_state('DONE')


def promote_old_to_new(old_d: Dimension, new_d: Dimension, P: Polyhedron):
    assert type(P) is Polyhedron
    assert old_d < new_d

    worklist_old[old_d].remove(P)

    assert P.dimension == old_d
    assert P[0] not in Poly

    if new_d <= MaxDim:
        assert P not in worklist_new[new_d]

        worklist_new[new_d].push(P)
        Poly[new_d][P[0]] = P
    else:
        # promoting beyond the outermost dimension == retiring
        retire(P)


def promote_new_to_old(new_d: Dimension, old_d: Dimension, P: Polyhedron):
    assert type(P) is Polyhedron
    assert new_d == old_d
    assert P.dimension == new_d - 1

    worklist_new[new_d].remove(P)
    Poly[new_d].pop(P[0])

    assert old_d <= MaxDim

    if old_d <= MaxDim:
        assert P not in worklist_old[old_d]

        P.normalize(old_d)
        worklist_old[old_d].push(P)
    else:
        # promoting beyond the outermost dimension == retiring
        retire(P)


def retire_early():
    """
      There are more polyhedra in worklists than the threshold allows.
      Retire some according to some more or less arbitrary criterion.

      This does not make folding imprecise, but reduces by how much we compress the stream
      of points.
    """

    # we retire the youngest polyhedron in the outermost dimension
    # argument: it will take the longest for it to merge with anyone again.
    for i in range(MaxDim + 1, -1, -1):
        if worklist_old[i]:
            # take last element of worklist.
            # we iterate worklists from front to back,
            # so the element whose next opportunity to absorb anyone is the furthest
            # is the future is the last one.
            P = worklist_old[i].pop()
            retire(P)
            break

        if worklist_new[i]:
            # take last element of worklist.
            # FIXME: does the same argument for worklist_old hold here?
            P = worklist_new[i].pop()
            retire(P)
            break


def retire(poly: 'Polyhedron'):
    global retired, num_live_polys
    num_live_polys -= 1
    retired.append(poly)
    log('# retire -->', poly)


def make_line(I: Vector, label: Vector) -> 'Polyhedron':
    P = Polyhedron(I)
    P.label = [Affine_Function(a) for a in label]
    return P


################################################################################
##### SUPPORT CODE

class Event:
    @staticmethod
    def make_point(point: Point) -> 'Event':
        assert type(point) is Point
        return Event(type='point', point=point)

    @staticmethod
    def make_iter(dim: int) -> 'Event':
        return Event(type='iter', dim=dim)

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __str__(self):
        if self.type == 'point':
            return f'point({self.point.IV}, {self.point.labels})'

        if self.type == 'iter':
            return f'iter({self.dim})'

        return repr(self)


class Event_Stream(NamedTuple):
    NumDims: int
    NumLabels: int
    Events: List[Event]

    @classmethod
    def from_ivs(clss, points: Iterable[Vector], context: str = None) -> 'Event_Stream':
        # TODO: allow specifying a label
        labels = Vector([0])

        context = context or ''

        it = iter(points)

        try:
            first_IV = next(it)
            first_point = Point(context, Vector(first_IV), labels)
        except StopIteration:
            # no points
            return Event_Stream(NumDims=0, NumLabels=0, Events=())

        stream = Event_Stream(
            NumDims   = len(first_point.IV),
            NumLabels = len(first_point.labels),
            Events    = [Event.make_point(first_point)],
        )

        prev_point = first_point

        for i, coordinates in enumerate(it, 2):
            coordinates = Vector(coordinates)
            curr_point  = Point(context, coordinates, labels)

            if prev_point:
                outermost_changed_dimension = clss.get_outermost_changed_dimension(prev_point.IV, coordinates)

                for dim in range(1, outermost_changed_dimension + 1):
                    stream.Events.append(Event.make_iter(dim))

            if len(curr_point.IV) != stream.NumDims:
                print(f'Point {i} [{coordinates}]: want {stream.NumDims} coordinate(s), have {len(curr_point.IV)}',
                      file=sys.stderr)
                exit(1)

            if len(curr_point.labels) != stream.NumLabels:
                print(f'Point {i} [{coordinates}]: want {stream.NumLabels} labels(s), have {len(curr_point.labels)}',
                      file=sys.stderr)
                exit(1)

            stream.Events.append(Event.make_point(curr_point))

            prev_point = curr_point

        # flush all worklists after points are done
        if prev_point:
          for dim in range(1, len(prev_point.IV) + 2):
            stream.Events.append(Event.make_iter(dim))

        return stream

    @classmethod
    def from_file(clss, input: IO[str]) -> Dict[str, 'Event_Stream']:
        streams = {}
        prev_point_per_stream = {}

        re_comment_or_blank = re.compile(r'^\s*(?:[#].*)?$')
        re_point            = re.compile(
            r'''
                ^
                    \s*
                    (?:
                        (?P<context>[\w\s]*) \s* :
                    )?
                    \s*
                    (?P<coordinates>
                        [+-]?\d+ (?: \s* , \s* [+-]?\d+ )*
                    )
                    \s*
                    -+>
                    \s*
                    (?P<labels>
                        [+-]?\d+ (?: \s* , \s* [+-]?\d+ )*
                    )
                    \s*
                $
            ''',
            re.VERBOSE,
        )

        lineno = 0
        line   = ''

        def emit_point(context: str, point: Point):
            if context not in streams:
                NumDims   = len(point.IV)
                NumLabels = len(point.labels)

                streams[context] = Event_Stream(NumDims=NumDims, NumLabels=NumLabels, Events=[])

            stream = streams[context]

            if len(point.IV) != stream.NumDims:
                print('on line', lineno,
                      f'({line!r}: want {stream.NumDims} coordinate(s), have {len(point.IV)}',
                      file=sys.stderr)
                exit(1)

            if len(point.labels) != stream.NumLabels:
                print('on line', lineno, f'({line!r}: want {stream.NumLabels} label(s), have {len(point.labels)}',
                      file=sys.stderr)
                exit(1)

            stream.Events.append(Event.make_point(point))

        def emit_iter(context: str, dim: int):
            assert context in streams, f'event stream {context!r} does not start with point!'

            streams[context].Events.append(Event.make_iter(dim))

        with input:
            for lineno, line in enumerate(input):
                if re_comment_or_blank.match(line):
                    ## comment or empty line
                    continue

                match = re_point.match(line)
                if not match:
                    print('syntax error in line', lineno, ':', f'{line!r}', file=sys.stderr)
                    exit(1)

                context     = match.group('context')
                coordinates = Vector(reversed(list(int(c.strip()) for c in match.group('coordinates').split(','))))
                labels      = Vector(int(c.strip()) for c in match.group('labels').split(','))

                if context is None:
                    context = ''

                prev_point = prev_point_per_stream.get(context)
                curr_point = Point(context, coordinates, labels)

                if prev_point:
                    outermost_changed_dimension = clss.get_outermost_changed_dimension(prev_point.IV, coordinates)

                    for dim in range(1, outermost_changed_dimension + 1):
                        emit_iter(context, dim)

                emit_point(context, curr_point)

                prev_point_per_stream[context] = curr_point

        for context, prev_point in prev_point_per_stream.items():
            # flush all worklists after points are done
            for dim in range(1, len(prev_point.IV) + 2):
                emit_iter(context, dim)

        return streams

    @staticmethod
    def get_outermost_changed_dimension(old_coords: Vector, new_coords: Vector) -> int:
        """
            Given the coordinates of the previous point (old) and the current point
            (new), detect the index of the outermost loop dimension that was iterated
            between the two points.

            NOTE: 0 is index of the *innermost* loop,
                  higher indices are inner loops,
                  i.e. the last entry is the *outermost* loop.
        """
        assert len(old_coords) == len(new_coords)

        num_dims: int = len(old_coords)

        for i in range(num_dims - 1, -1, -1):
            if old_coords[i] != new_coords[i]:
                return i + 1

        raise ValueError('same point appeared twice in stream???', [old_coords, new_coords])

    def __iter__(self):
        return self

    def __next__(self) -> Tuple[Dimension, Point]:
        return next(self._points)


def print_state(*tag):
    global context

    if context:
        tag = (f'(Context: {context!r})', ) + tag

    description = ' '.join(map(str, tag))

    ## latex output
    if Logging.track_state:
        image = images.image(
            description        = description,
            changed_dimensions = [Logging.dim],
        )

        if Logging.point is not None:
            image.add_poly(images.new_point(Logging.point.IV))

            Logging.max_coords = Vector(
                [
                    max(Logging.max_coords[i], Logging.point.IV[i]) for i in range(len(Logging.max_coords))
                ]
            )

        for i in range(len(worklist_old)):
            for P in worklist_new[i]:
                image.add_poly(
                    images.new_poly(i, copy.deepcopy(P.affine_functions), *reversed([point for point in P]))
                )
            for P in worklist_old[i]:
                image.add_poly(
                    images.old_poly(i, copy.deepcopy(P.affine_functions), *reversed([point for point in P]))
                )
        for P in retired:
            image.add_poly(
                images.retired_poly(P.dimension, copy.deepcopy(P.affine_functions),
                                    *reversed([point for point in P]))
            )

        Logging.images.append(image)

    if Logging.verbose:
        ## logging
        log('##', description)
        log('#', 'Poly', '[' + ', '.join(['{' + ', '.join(f'{p[0]}: {p[1]}' for p in P.items()) + '}' for P in Poly]) + ']')
        for i in range(len(worklist_old)):
            log('# ', 'new', i, '[' + ', '.join(map(str, worklist_new[i])) + ']')
            log('# ', 'old', i, '[' + ', '.join(map(str, worklist_old[i])) + ']')
        for P in retired:
            log('# ', 'retired', P)
        log('#-----------')


def log(*args, end=None):
    """
        Log message to stderr
    """
    if Logging.verbose:
        print(*args, end=end, file=sys.stderr)


def copy_to(src: list, dst: list):
    """
        Clear `dst` and then copy contents of `src` into `dst`.
    """
    dst.clear()
    for elem in src:
        dst.append(elem)


def main():
    """
        Main function launching the benchs
    """

    # Argument parser
    parser = argparse.ArgumentParser(description='Run the modified version of JSC including polly')
    parser.add_argument(
        'input',
        help='Input file to read points from',
        nargs='?',
        type=argparse.FileType('r'), default=sys.stdin,
    )
    parser.add_argument(
        '--widening',
        help='allow label functions to merge when they should not',
        action='store_true', default=False,
    )
    parser.add_argument(
        '--threshold',
        help='max size of worklist before we give up (? I think ?)',
        type=int, default=42,
    )
    parser.add_argument(
        '--render-latex', '--latex',
        help='print latex showing state of folding at each step',
        action='store_true', default=False,
    )
    parser.add_argument(
        '--render-matplot', '--matplot',
        help='Show state of folding at each step using matplotlib',
        action='store_true', default=False,
    )
    parser.add_argument(
        '--render-tail',
        help='print latex showing only the last N steps of folding',
        type=int, default=0,
    )
    parser.add_argument(
        '--verbose', '-v',
        help='Log state of algorithm on every folding step to stdout',
        action='store_true', default=False,
    )
    args = parser.parse_args()

    track_state = (args.render_latex or args.render_matplot)

    if args.render_tail and not track_state:
        args.render_latex = True

    streams = Event_Stream.from_file(args.input)

    for context, stream in sorted(streams.items()):
        MaxDim = stream.NumDims
        events = stream.Events

        init_fold(context, MaxDim, widen_affine_functions_=args.widening, verbose=args.verbose, track_state=track_state)
        assert Logging.dim == 0

        log('#=====================#')
        log('# FOLD CONTEXT:', repr(context), '#')
        log('#=====================#')

        fold(
            MaxDim=MaxDim,
            addDims=False,
            threshold=args.threshold,
            events=events,
        )

        if not Logging.track_state and not Logging.verbose:
            # print at least the final state if nothing else
            Logging.verbose = True
            print_state('Result:')
            Logging.verbose = False

        # print latex
        if Logging.track_state:
            max_coords = Logging.max_coords

            # only show last latex_tail frames
            frames = Logging.images[-args.render_tail:]

            doc = images.document(
                images = frames,
            )

            if args.render_latex:
                beamer.plot_document(doc, sys.stdout)
            if args.render_matplot:
                matplot.plot_document(doc)
