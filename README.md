## Python Folding

Python implementation of the geometric folding trace compression algorithm used in MICKEY/POLY-PROF.

The main mickey code base can be found [here](https://gitlab.inria.fr/fgruber/mickey).
A detailed explanation of the geometric folding trace compression algorithm can be found [here](https://hal.inria.fr/hal-01967828v2).

The goal of this project is to have a clean Python implementation of the geometric folding trace compression algorithm.
It is mostly here to be easy to understand and experiment with and notably does not contain some performance optimizations of the C++ version (i.e. the special casing for points in innermost loops, ...)


## Get started

To run an example:
```bash
# install required python packages
pip3 install -r requirements.txt

# read points to fold from file
python3 -m folding examples/triangle-and-rectangles.txt

# read points to fold from stdin
examples/emit_points.py -c A -x 2 -- -c B -x 3 | python3 -m folding

# create a PDF file describing each step of the algorithm
python3 -m folding --latex examples/triangle-and-rectangles.txt >slides.tex
pdflatex slides.tex # <- takes a while
```

You can very easily package the folding code in an executable archive
```bash
make folder

# now you can replace 'python3 -m folding' with './folder' in the examples above
./folder examples/triangle-and-rectangles.txt
```

## License

Copyright (C) 2018-2019 Université Grenoble Alpes
All rights reserved.

This software may be modified and distributed under the terms
of the BSD license.  See the LICENSE.txt file for details.
