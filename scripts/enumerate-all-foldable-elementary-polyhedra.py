#!/usr/bin/env python3
## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

"""
  This should run folding over all polyhedra that folding can fold in one piece.
  If you find another one, call Manu.

  Just FYI: This currently does not work.
            I (Fabian), think we need to add constraints on the ratio of imin/jmin/kmin and the maxs respectively.
"""

import argparse
import io
import itertools
import math
import os
import os.path
import pathlib
import shlex
import shutil
import subprocess
import sys
import tempfile
from typing import *

try:
  import folding
  import folding.images
  import folding.beamer_images
  import folding.matplotlib_images
except ModuleNotFoundError:
  path = pathlib.Path(__file__)
  path = path.parent.parent
  if (path / 'folding').is_dir():
    sys.path.append(str(path))
    import folding
    import folding.images
    import folding.beamer_images
    import folding.matplotlib_images
  else:
    raise


def fold(
  i_min: int,
  j_min: int,
  k_min: int,
  i_max: int,
  j_max: int,
  k_max: int,
  d1: int,
  d2: int,
  d3: int,
  d4: int,
  d5: int,
  d6: int,
  comment: Callable[..., None],
  point: Callable[..., None],
):
  for i in range(i_min, i_max + 1):
    comment('# BGN i', i)

    for j in range(j_min + d1 * i, j_max + d2 * i + 1):
      comment('#   BGN j', j)

      for k in range(k_min + d3 * i + d4 * j, k_max + d5 * i + d6 * j + 1):
        comment('#     BGN k', k)
        point(i, j, k)

        assert i_min <= i <= i_max
        assert j_min + d1 * i <= j <= j_max + d2 * i
        assert k_min + d3 * i + d4 * j <= k <= k_max + d5 * i + d6 * j

      comment(f'#   END j={j} (i={i} k={k})')

    comment(f'# END i={i} (j={j} k={k})')


def fold_and_emit_pdf(
  i_min: int,
  j_min: int,
  k_min: int,
  i_max: int,
  j_max: int,
  k_max: int,
  d1: int,
  d2: int,
  d3: int,
  d4: int,
  d5: int,
  d6: int,
  dest_dir: pathlib.Path,
  tmpdir: pathlib.Path,
  render_pdf: bool,
  render_viewer: bool,
  view_failures: bool,
):
  points = []

  log_points_to_stdout = False

  def comment(*msg):
    if log_points_to_stdout:
      print(*msg)
    pass

  def point(*ijk):
    if log_points_to_stdout:
      print(','.join(map(str, ijk)), '--->', 0)
    points.append(tuple(reversed(ijk)))

  fold(
    i_min   = i_min,
    j_min   = j_min,
    k_min   = k_min,
    i_max   = i_max,
    j_max   = j_max,
    k_max   = k_max,
    d1      = d1,
    d2      = d2,
    d3      = d3,
    d4      = d4,
    d5      = d5,
    d6      = d6,
    comment = comment,
    point   = point,
  )

  def coeff(n: int):
    if n >= 0:
      return '+' + str(n)
    else:
      return str(n)

  name = f'-i-min={i_min} -i-max={i_max} -j-min={j_min} -j-max={j_max} -k-min={k_min} -k-max={k_max} -d1={coeff(d1)} -d2={coeff(d2)} -d3={coeff(d3)} -d4={coeff(d4)} -d5={coeff(d5)} -d6={coeff(d6)}'

  if not points:
    print('warning: case', repr(name), 'contains zero points', file=sys.stderr)

  stream = folding.Event_Stream.from_ivs(points)

  print(
    '#', 'folding', name, f'(npoints={len(points)})',
    flush=True, end='',
    file=sys.stderr
  )

  folding.init_fold(
    context_='',
    MaxDim_=stream.NumDims,
    widen_affine_functions_=False,
    verbose=False,
    track_state=False,
  )

  folding.fold(
    MaxDim=stream.NumDims,
    addDims=False,
    threshold=math.inf,
    events=stream.Events,
  )

  failed: bool

  if len(folding.retired) == 1:
    failed = False
    tag    = 'PASS'
  else:
    failed = True
    tag    = f'FAIL! ({len(folding.retired)} pieces)'

  if d1 == +1 and d3 == +1 and d4 == +1:
    assert failed
  if d1 == +1 and d3 == -1 and d4 == -1:
    assert failed
  if d1 == -1 and d3 == +1 and d4 == -1:
    assert failed
  if d1 == -1 and d3 == -1 and d4 == +1:
    assert failed

  print(' =>', tag, file=sys.stderr)

  if not render_pdf and not render_viewer and not view_failures:
    return

  folding.Logging.track_state = True
  folding.print_state(tag)

  doc = folding.images.document(
    images = folding.Logging.images,
  )

  pdf_file = None

  if render_pdf:
    latex = io.StringIO()
    folding.beamer_images.plot_document(doc, latex)

    ret = subprocess.run(
      ['chronic', 'pdflatex'],
      input=latex.getvalue(),
      universal_newlines=True,
      cwd=tmpdir,
    )
    if ret.returncode != 0:
      print(latex.getvalue())
      exit(ret.returncode)

    tmp = tmpdir / 'texput.pdf'
    if failed:
      out_dir = dest_dir / 'fail'
    else:
      out_dir = dest_dir / 'pass'

    os.makedirs(out_dir, exist_ok=True)

    pdf_file = out_dir / ('RENDER ' + name + '.pdf')

    shutil.move(tmp, pdf_file)

  if render_viewer or (failed and view_failures):
    folding.matplotlib_images.plot_document(doc, name=name)

  return pdf_file


def main(argv = None):
  parser = argparse.ArgumentParser(
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
  )

  parser.add_argument(
    '--dest-dir',
    help='Directory to store rendered PDFs in (will be created if necessary)',
    type=pathlib.Path,
    required=False,
  )
  parser.add_argument(
    '--render-pdf',
    help='Render PDF showing final polyhedron',
    action='store_true',
    default=False,
  )
  parser.add_argument(
    '--render-viewer',
    help='Open interactive viewer that shows folded polyhedron',
    action='store_true',
    default=False,
  )
  parser.add_argument(
    '--view-failures',
    help='Open interactive viewer for cases where folding failed',
    action='store_true',
    default=False,
  )

  for n in 'ijk':
    parser.add_argument(f'-{n}-min', type=int, default=0)
    parser.add_argument(f'-{n}-max', type=int, default=10)

  for n in range(1, 7):
    parser.add_argument(f'-d{n}', type=int, default=None)

  args = parser.parse_args(argv)

  if args.render_pdf:
    if args.dest_dir is None:
      parser.error('With --render-pdf the following argument is required: --dest-dir')

    tmpdir   = pathlib.Path(tempfile.mkdtemp(prefix='folding.'))
    dest_dir = args.dest_dir.absolute()
  else:
    tmpdir = None

  ds = []
  for i in range(1, 7):
    d = getattr(args, f'd{i}')
    if d is None:
      d = [+0, +1, -1]
    else:
      d = [d]
    ds.append(d)

  pdfs = set()

  d1 = d2 = d3 = d4 = d5 = d6 = 0
  for d1, d2, d3, d4, d5, d6 in itertools.product(*ds):
    OFFSET = 0
    LEN    = 4

    pdf = fold_and_emit_pdf(
      i_min   = args.i_min,
      j_min   = args.j_min,
      k_min   = args.k_min,
      i_max   = args.i_max,
      j_max   = args.j_max,
      k_max   = args.k_max,

      d1      = d1,
      d2      = d2,
      d3      = d3,
      d4      = d4,
      d5      = d5,
      d6      = d6,

      render_pdf    = args.render_pdf,
      render_viewer = args.render_viewer,
      view_failures = args.view_failures,
      dest_dir      = args.dest_dir,
      tmpdir        = tmpdir,
    )

    if args.render_pdf:
      if pdf in pdfs:
        print('error: Duplicate output file: {pdf}', file=sys.stderr)
        exit(1)
      pdfs.add(pdf)

      print(shlex.quote(str(pdf)))


if __name__ == '__main__':
  try:
    main()
  except KeyboardInterrupt:
    pass
