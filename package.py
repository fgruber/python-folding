#!/usr/bin/env python3
# -*- coding: utf-8 -*-
## Copyright (C) 2018-2019 Université Grenoble Alpes
## All rights reserved.
##
## This software may be modified and distributed under the terms
## of the BSD license.  See the LICENSE.txt file for details.

"""
  Package folding code into an executable python archive.
"""

import argparse
import zipapp
import zipfile
import pathlib
import sys
import io
import stat

import folding


def main():
  parser = argparse.ArgumentParser(
    description='''
      Package folding code into an executable python archive.
    '''
  )

  parser.add_argument(
    '-o', '--output',
    help='Pack to write package to',
    type=pathlib.Path,
    default=pathlib.Path(__file__).parent / 'folder',
  )
  parser.add_argument(
    '-p', '--python',
    help='The name of the Python interpreter to use',
    default=sys.executable,
  )
  parser.add_argument(
    '-v', '--verbose',
    help='Print verbose information',
    action='store_true', default=False,
  )

  args = parser.parse_args()

  with io.BytesIO() as buf:
    ## write shebang
    buf.write(b'#!' + args.python.encode(zipapp.shebang_encoding) + b'\n')

    src = pathlib.Path(folding.__file__).parent

    with zipfile.ZipFile(buf, 'w', compression=zipfile.ZIP_DEFLATED) as zip:
      for path in src.rglob('*'):
        if path.suffix != '.py':
          continue

        zip_path = 'folding' / path.relative_to(src)
        if args.verbose:
          print('ADD', path, '->', zip_path)
        zip.write(path, zip_path.as_posix())

      zip.writestr('__main__.py', ZIP_MAIN)

    if args.verbose:
      print('WRITE', args.output)

    with open(args.output, 'wb') as fd:
      fd.write(buf.getbuffer())

    args.output.chmod(args.output.stat().st_mode | stat.S_IEXEC)


ZIP_MAIN = b'''\
# -*- coding: utf-8 -*-

import folding.__init__

folding.__init__.main()

'''

if __name__ == '__main__':
  main()
